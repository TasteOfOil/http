const http = require('http');
const path = require('path');
const fs = require('fs');

const server = http.createServer((req,res)=>{
    console.log(req.url);
    if(req.url === '/'){
        fs.readFile(path.join(__dirname,'HtmlPages','homepage.html'),(err,data)=>{
            if(err) throw err
            res.end(data);
            console.log("We on Homepage");
        })
    }
    else if(req.url === '/about'){
        fs.readFile(path.join(__dirname,'HtmlPages','aboutapp.html'),(err,data)=>{
            if(err) throw err
            res.end(data);
            console.log("We on About");
        })
    }
    else {
        fs.readFile(path.join(__dirname,'HtmlPages','err.html'),(err,data)=>{
            if(err) throw err
            res.end(data);
            console.log("Error 404: page not found");
        })
    }
});

const PORT = process.env.PORT||2000;
server.listen(PORT, ()=>{
    console.log(`Server started. PORT: ${PORT}`);
})


